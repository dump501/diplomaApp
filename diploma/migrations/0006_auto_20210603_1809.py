# Generated by Django 3.1.7 on 2021-06-03 16:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('diploma', '0005_owner_placeob'),
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='diploma',
            name='department',
            field=models.ForeignKey(default=True, on_delete=django.db.models.deletion.CASCADE, to='diploma.department'),
        ),
    ]
