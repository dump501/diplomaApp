# Generated by Django 3.1.7 on 2021-05-28 18:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Diploma_type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Institute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Diploma',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250)),
                ('content', models.TextField()),
                ('delivry_date', models.DateField()),
                ('identification', models.DateTimeField(auto_now_add=True)),
                ('diploma_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diploma.diploma_type')),
                ('institute', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diploma.institute')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diploma.owner')),
            ],
        ),
    ]
