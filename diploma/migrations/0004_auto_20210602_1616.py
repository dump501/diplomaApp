# Generated by Django 3.1.7 on 2021-06-02 14:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diploma', '0003_auto_20210602_1615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='diploma',
            name='content',
        ),
        migrations.RemoveField(
            model_name='diploma',
            name='title',
        ),
    ]
