from django.contrib import admin

# Register your models here.
from .models import Owner
from .models import Institute
from .models import Diploma_type
from .models import Diploma
from .models import Department

admin.site.register(Owner)
admin.site.register(Institute)
admin.site.register(Diploma_type)
admin.site.register(Diploma)
admin.site.register(Department)
