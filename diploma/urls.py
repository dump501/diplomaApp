from django.urls import path
from . import views


app_name = 'communique'

urlpatterns = [
    path('', views.index, name='index'),
    path('generate_pdf', views.tool, name="generatePdf"),
    path('tool', views.tool, name="tool")
]
