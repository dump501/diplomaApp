from django.db import models

# Create your models here.
class Owner(models.Model):
    name = models.CharField(max_length=250)
    mention = models.CharField(max_length=20)
    dob = models.DateField(null=True)
    placeob = models.CharField(max_length=30, null=True)
    matricule = models.CharField(max_length=20, null=True)


    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.name or ''

class Institute(models.Model):
    name = models.CharField(max_length=250)
    shortName = models.CharField(max_length=20, null=True)

    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.shortName or ''

class Department(models.Model):
    name = models.CharField(max_length=200, null=True)

    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.name or ''

class Diploma_type(models.Model):
    name = models.CharField(max_length=250)
    longName = models.CharField(max_length=200, null=True)

    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.name or ''

class Diploma(models.Model):
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE)
    diploma_type = models.ForeignKey(Diploma_type, on_delete=models.CASCADE)
    delivry_date = models.DateField()
    department = models.ForeignKey(Department, on_delete=models.CASCADE, default=True)
    identification = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Unicode representation of MODELNAME."""
        return self.owner.name or ''
