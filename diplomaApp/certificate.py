from reportlab.lib.pagesizes import letter
from reportlab.lib.pagesizes import landscape
from reportlab.pdfgen import canvas
from reportlab.platypus import Image, Paragraph
#import pandas as pd
from reportlab.graphics import shapes, renderPDF
from reportlab.graphics.barcode import qr

#write filepath here
# excel_file = 'names.xlsx'
# #names column name
# column_name = 'name'
# edit these two to change the certificate text
title = 'UNIVERSITY OF BAMENDA'
schoolName = 'Higher Technical Teacher Trainning College'
schoolShortName = "HTTTC"

studentName = "Tsafack Djiogo Fritz Albin"
documentType = "DIPET I"
mention = "Excellent"
qrData = "hello-dump"

#logo, change the logo file name here, use only JPG 
logo = 'media/img/logo_uba.jpg'
viceChancellorsign = 'media/img/signature1.jpg'
directorsign = 'media/img/signature3.jpg'


def generatePdf(diploma):
    # data = pd.read_excel(file_name)
    # make sure the column name for the names in 'name'
    pdf_file_name = 'media/pdf/' + str(diploma['studentName']) +'.pdf'
    make_certificate(diploma, pdf_file_name)
    # for name in data[column_name]:
    #     pdf_file_name = "media/pdf/" + name
    #     pdf_file_name = name + '.pdf'
    #     make_certificate(name, pdf_file_name)
        
def make_certificate(diploma, pdf_file_name):

    schoolName = diploma['schoolName']
    schoolShortName = diploma['schoolShortName']

    studentName = diploma['studentName']
    dob = diploma['dob']
    matricule = diploma['matricule']
    documentType = diploma['documentType']
    diplomaLongName = diploma['diplomaLongName']
    departement = diploma['department']
    mention = diploma['mention']
    qrData = diploma['qrData']
    c = canvas.Canvas(pdf_file_name, pagesize=landscape(letter))
    
    #document title
    c.setFont('Times-Roman', 50, leading=None)
    c.drawCentredString(415, 550, title)

    #logo
    c.drawImage(logo, 350, 420, width=100, height=100)

    #this certifi ..
    l1 = "This certify that " + studentName + " born on " + str(dob) + " matricule: " + matricule
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 390, l1)

    #school full name
    #c.setFont('Times-Italic', 25, leading=None)
    #c.drawCentredString(450, 420, schoolName)

    #school short name
    #c.setFont('Times-Bold', 20, leading=None)
    #c.drawCentredString(450, 390, "( " + schoolShortName + " )")

    #diploma content
    lineOne = "having fullfild all requirement of the university and satisfactorilly completed the prescribed"
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 360, lineOne)

    lineTwo = "course of study in school " + schoolName + "(" + schoolShortName + ")"
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 330, lineTwo)

    lineThree = "has under the autority of senate been awarded the"
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 300, lineThree)

    l2 = diplomaLongName
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 270, l2)

    l3 = departement
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 240, l3)

    l4 = "Grade: " + mention
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 210, l4)
    
    lineFour = "Minister of Heigher education                                     The Vice CHancellor"
    c.setFont('Times-Roman', 20, leading=None)
    c.drawCentredString(415, 150, lineFour)
    #director, HOD, and the vice chancelor signature
    c.drawImage(viceChancellorsign, 100, 25, width=100, height=100)
    # c.drawImage(logo, 10, 520, width=100, height=100)

    #draw qr code
    drawing_obj = shapes.Drawing()
    qrcode = generateQr(qrData)
    drawing_obj.add(qrcode)
    renderPDF.draw(drawing_obj, c, 600, 420)
    #renderPDF.draw(drawing_obj, c, 700, 5)

    #diploma content
    # content = Paragraph("")
    # content.drawOn(c, 100, 230)

    # c.setFont('Times-Italic', 20, leading=None)
    # c.drawCentredString(415, 350, second_line)
    # #name
    # c.setFont('Times-BoldItalic', 45, leading=None)
    # c.drawCentredString(415, 295, name)
    
    
    #save pdf
    c.save()


def generateQr(data):
    text = data
    qrcode = qr.QrCodeWidget(text)
    return qrcode
    