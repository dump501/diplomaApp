from django.shortcuts import render
from django.http import HttpResponse
from .certificate import generatePdf
from diploma.models import *
from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'diploma/index.html')

def test(request):
    # generatePdf('dump501')
    for diploma in Diploma.objects.all():
        diplomaData = {}
        diplomaData['schoolName'] = diploma.institute.name
        diplomaData['schoolShortName'] = diploma.institute.shortName
        diplomaData['dob'] = diploma.owner.dob
        diplomaData['matricule'] = diploma.owner.matricule
        diplomaData['studentName'] = diploma.owner.name
        diplomaData['documentType'] = diploma.diploma_type.name
        diplomaData['diplomaLongName'] = diploma.diploma_type.longName
        diplomaData['department'] = diploma.department.name
        diplomaData['mention'] = diploma.owner.mention
        diplomaData['qrData'] = ""
        diplomaData['qrData'] = str(diplomaData['schoolShortName']) + "_" + str(diplomaData['studentName']) + "_" + str(diplomaData['documentType']) + "_" + str(diplomaData['mention'])
        generatePdf(diplomaData)

    return HttpResponse('PDF generated successfully <a href="http://127.0.0.1:8000/admin">ok take to admin page</a>')

def tik(request):

    diplomaData = {}
    diplomaData['schoolName'] = "hel sdklg sdfgjk qpmlsou dfqsiduk f ufh"
    diplomaData['schoolShortName'] = "HTTTC"
    diplomaData['studentName'] = "Tsafack djiogo fritz albin"
    diplomaData['documentType'] = "DIPET I"
    diplomaData['mention'] = "Excellent"
    diplomaData['qrData'] = ""
    diplomaData['qrData'] = str(diplomaData['schoolName']) + "_" + str(diplomaData['schoolShortName']) + "_" + str(diplomaData['documentType']) + "_" + str(diplomaData['mention'])
    generatePdf(diplomaData)

    return HttpResponse('PDF generated successfully <a href="http://127.0.0.1:8000/admin">ok take to admin page</a>')